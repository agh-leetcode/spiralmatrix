import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        /*int[][]matrix = new int[][]{
                {1,2,3},{4,5,6},{7,8,9}
        };*/

        int[][]matrix = new int[][]{
                {1,2,3,4},{5,6,7,8},{9,10,11,12}
        };

        for (int i=0; i<matrix.length;i++){
            for (int j=0; j<matrix[0].length;j++){
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        for (Integer integer : new Solution().spiralOrder(matrix)) {
            System.out.print(integer + " ");
        }
    }

    private List<Integer> spiralOrder(int[][] matrix) {

        if(matrix.length==0)
            return new ArrayList();

        int rows = matrix.length;
        int columns = matrix[0].length;
        int currentRaw = 0;
        int currentColumn = 0;
        int i;

        List<Integer> result = new ArrayList<>();

        while(currentRaw<rows && currentColumn<columns){
            for(i = currentColumn; i<columns; i++){
                result.add(matrix[currentRaw][i]);
            }
            currentRaw++;

            for(i = currentRaw; i < rows; i++){
                result.add(matrix[i][columns-1]);
            }
            columns--;

            if(currentRaw<rows){
                for(i = columns-1; i >= currentColumn; i--){
                    result.add(matrix[rows-1][i]);
                }
                rows--;
            }

            if(currentColumn<columns){
                for(i = rows-1; i >= currentRaw; i--){
                    result.add(matrix[i][currentColumn]);
                }
                currentColumn++;
            }
        }
        return result;

    }
}
